$(document).ready(function () {

    var $jq = jQuery.noConflict();

    //Top Search
    $jq(".searchbar input").keypress(function () {
        $jq(".search-clear").show();
    });
    $jq(".search-clear").on('click', function () {
        $jq(".searchbar input").val("").focus();
        $jq(".searchbar-mobile").slideToggle();
    });

    //Mobile search
    $jq(".search-click").on('click', function () {
        $jq(".searchbar-mobile").slideToggle();
    });

    //Search Results Page
    $jq(".types").keypress(function (e) {
        if (e.which == 13) {
            window.location.href = "search-results.html";
        }
    });

    // Toggle classes in body for syncing sliding animation with other elements
    $jq('#mainNav').on('show.bs.collapse', function () {
        $jq('body').addClass('menu-slider');
    }).on('hide.bs.collapse', function () {
        $jq('body').removeClass('menu-slider');
    });

    //Main Navigation
    $jq(document).on('click', '.main-navigation .dropdown-menu', function (e) {
        e.stopPropagation();
    });

    //Stop Actions
    $jq(".favourite, .actions-on-card").on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    // Back To Top
    $jq(window).scroll(function () {
        if ($jq(this).scrollTop() < 100) {
            $jq('#backtotop').fadeOut();
        } else {
            $jq('#backtotop').fadeIn();
        }
    });
    $jq('#backtotop').on('click', function () {
        $jq('html, body').animate({scrollTop: 0}, 'fast');
        return false;
    });

    //Fixed Filter
    $jq(window).bind('scroll', function () {
        var filHeight = 300;
        ($jq(window).scrollTop() > filHeight) ? $jq('.filter-fixed').addClass('filter-fixed-position') : $jq('.filter-fixed').removeClass('filter-fixed-position');
    });

    // Main function to check screen width
    function ele_adjust() {
        var conwid = $jq(".container").width();
        if (conwid > 700) {
            footerweb();
        } else {
            footermobile();
        }
    }

    // Removing Accordion in Web
    function footerweb() {
        $jq(".menutoacc, .footersub-acc").removeClass("accordion");
        $jq(".menutoacc ul.acclist, .footersub-acc ul.footersub-list").removeAttr("style");
        $jq(".clickacc span, .subclickacc span").css('display', "none");

        //Top Menu
        $jq('.main-navigation li a').addClass('disabled');

        $jq("#pay-cards").insertAfter(".location-address");

        //Insert Continue Shopping button after checkout
        $jq("#con-shopping").insertAfter(".emicodeenter");

        //Sticky Menu
        $jq(window).bind('scroll', function () {
            var navHeight = 125; // custom nav height
            ($jq(window).scrollTop() > navHeight) ? $jq('.fixedmenu').addClass('goToTop') : $jq('.fixedmenu').removeClass('goToTop');
        });

        /*NiceScroll*/
        $(".vscroll").niceScroll({cursorcolor:"#ccc"});
    }

    // Applying Accordion in Mobile
    function footermobile() {
        $jq(".menutoacc, .footersub-acc").addClass("accordion");
        $jq(".clickacc span, .subclickacc span").css('display', "block");
        //Footer First Level
        $jq(".menutoacc.accordion div.clickacc").click(function () {
            if (false == $jq(this).next('ul').is(':visible')) {
                $jq('.menutoacc.accordion ul.acclist').slideUp(300);
                $jq(".clickacc span i").removeClass("fa-minus").addClass("fa-plus");
            }
            $jq(this).next('ul').slideDown(300);
            $jq(this).find("i").removeClass("fa-plus").addClass("fa-minus");
            return false;
        });
        //Footer Second Level
        $jq(".footersub-acc.accordion div.subclickacc").click(function () {
            if (false == $jq(this).next('ul').is(':visible')) {
                $jq('.footersub-acc.accordion ul.footersub-list').slideUp(300);
                $jq(".subclickacc span i").removeClass("fa-angle-up").addClass("fa-angle-down");
            }
            $jq(this).next('ul').slideDown(300);
            $jq(this).find("i").removeClass("fa-angle-down").addClass("fa-angle-up");
            return false;
        });
        //Top Menu
        $jq('.main-navigation li a').removeClass('disabled');
        
        //Insert the payment cards in front of Copyrights
        $jq("#pay-cards").insertAfter("p.copyright");

        //Insert Continue Shopping button after checkout
        $jq("#con-shopping").insertAfter("#checko");

        //Sticky Menu
        $jq('.fixedmenu').removeClass('goToTop');
        $jq(window).bind('scroll', function () {
            var navHeight = 125; // custom nav height
            ($jq(window).scrollTop() > navHeight) ? $jq('.fixedmenu').removeClass('goToTop') : $jq('.fixedmenu').removeClass('goToTop');
        });
    }

    // Run Main function to check screen width
    ele_adjust();
    $jq(window).resize(ele_adjust);

    //Favourite
    $jq('.favourite').on('click', function () {
        $jq(this).toggleClass('selected');
    });

    //See More Items
    $('.see-more-click').on('click', function () {
        $(this).parent().prev().find('.see-more-items').slideToggle(400);
        var $el = $(this).children('p:first-child');
        $el.text($el.text() == "See Less" ? "See More" : "See Less");
        $(this).children('p:last-child').toggleClass('glyphicon-menu-up');
    });

    //Select Filters
    $jq('.selectfilters').on('click', function () {
        $jq('.content-filters').slideToggle(400);
        $jq('.content-compare').slideUp(400);
        $jq(this).find('.filter-icon').toggleClass("selected");
    });
    $jq('.close-filter').on('click', function () {
        $jq('.content-filters').slideUp(400);
    });
    //select color add active
    $jq('.colors-list li').on('click', function () {
        $jq(this).toggleClass('active');
    });

    //Compare
    $jq('.compare-right').on('click', function () {
        $jq('.content-compare').slideToggle(400);
        $jq('.content-filters').slideUp(400);
        $jq(this).children('span:first-child').toggleClass("selected");
        $jq(this).children('span:last-child').toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
    });

    //Toggle Save and Edit in Profile
    $jq('.pf-edit').on('click', function () {
        $jq(this).toggleClass('savefile');
        $jq(this).find('.pficon').toggleClass('pficon-save').toggleClass('pficon-edit');
        $jq('.user-info1 p.input-value,.user-info1 input').toggle();
    });
    $jq('.user-edit1').on('click', function () {
        $jq(this).toggleClass('savefile');
        $jq(this).find('.pficon').toggleClass('pficon-save').toggleClass('pficon-edit');
        $jq(this).parents('.shipping-user-block').find('.user-shipping p.input-value,.user-shipping input,.user-shipping textarea').toggle();
    });

    //Title Editing
    $jq('.dtitle-edit').on('click', function () {
        var $edititle = $jq(this).sibling('.mydesign-title'), isEditable = $edititle.is('.editable');
        //alert('end1');
        $jq(this).siblings('.mydesign-title').prop('contenteditable', !isEditable).toggleClass('editable');
    });

    //Toggle password to edit
    $jq('.settingspwd-save').on('click', function () {
        $jq('.default-pwd, .news-pwd').toggle();
    });

    // Add to Cart Top / Add to Wishlist Top
    $jq('.btnCartWishList').on('click', function () {
        console.log($jq(this).hasClass("addwishlist"));
        $('html, body').animate({scrollTop: 0}, 'fast');
        if($jq(this).hasClass("addwishlist")){
            $jq('.wishview').show();
            $jq('.cartview').hide();
        }
        else{
            $jq('.cartview').show();
            $jq('.wishview').hide();
        }
        return false;
    });
    $jq('.closeme').on('click', function () {
        $jq(this).parent().hide();
    });

    //Select Cash on delivery / Pay through EMI service
    $('.radioclick').on('ifChecked', function () {
        $('.payment-explain').hide();
        $(this).parent().parent().next().slideToggle(400);
    });

    //smooth page scroll to an anchor
    $jq('.gotovariation').on('click', function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $jq('html, body').stop().animate({
            'scrollTop': $target.offset().top - 85
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

    //Switching back to booking point in book appointment
    $jq('.back-booking').on('click', function(){
        $jq('.booking-tags, .booking-wishlist').hide();
        $jq('.main-booking').show();
    });
    $jq('.sendword-you').on('click', function(){
        $jq('.main-booking, .booking-wishlist').hide();
        $jq('.booking-tags').show();
    });
    $jq('.sendwish-store').on('click', function(){
        $jq('.booking-tags, .main-booking').hide();
        $jq('.booking-wishlist').show();
        $('.slider1').resize();
    });

    //Tag save slection
    $jq('.booking-tagsinfo li a').on('click', function () {
        $jq(this).toggleClass('active');
    });

    //select the look in details
    $jq('.looks-refer').on('click', function(){
        $jq(this).find('.store-icon').toggleClass('pin-off').toggleClass('pin-lock');
    });

    //Select All Pins
    $jq('.pin-all').on('click', function(){
        $jq('.booking-wishlistinner').find('.store-icon').removeClass('pin-off').addClass('pin-lock');
    });

    /*Signup And login functions in Index.html*/
    $jq('.loginsignup-toggle').on('click', function(){
        $jq('.login-form-main, .signup-form').toggle();
    });

    /*Intialize Flex slider in popups*/
    $('.modal').on('shown.bs.modal', function () {
        initCloseModal();
        $('.swatches-samples, .carousel1, .fullslider1, .slider1').resize();
    });

    //Price Slider
    $(".slider-price").slider({
        range: true,
        min: 0,
        max: 200000,
        values: [75, 100000],
        slide: function (event, ui) {
            $jq('#amount').val("₹ " + ui.values[0]);
            $jq('#amount1').val("₹ " + ui.values[1]);
        }
    });
    $("#amount").val("₹ " + $(".slider-price").slider("values", 0));
    $("#amount1").val("₹ " + $(".slider-price").slider("values", 1));


    //Accordian Plus Minus
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i")
            .toggleClass('det-acc det-acc-minus');
    }
    $('.productaccordian .panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.productaccordian .panel-group').on('shown.bs.collapse', toggleIcon);

    //Accordian arrow
    function toggleIconArrow(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i")
            .toggleClass('glyphicon-menu-up glyphicon-menu-down');
        $(e.target).prev('.panel-heading').toggleClass('highlight');
        $('.diff-material').resize();
    }
    $('.prodcustom-accordian .panel-group').on('hidden.bs.collapse', toggleIconArrow);
    $('.prodcustom-accordian .panel-group').on('shown.bs.collapse', toggleIconArrow);

    // and show any FlexSliders on tab change
    $('.tabmaterial a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        $('.diff-material').resize();
    });

    //Fixed Column Table
    var $table = $jq('.comparetable');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('td:not(:first-child)').remove();
    $fixedColumn.find('tr').each(function (i) {
        $jq(this).height($table.find('tr:eq(' + i + ')').height());
    });

    //Review toggle
    $jq('.writereview a').on('click', function () {
        $jq('.reviewopen').slideToggle(500);
        $jq(this).hide();
    });

    // load a popup after a window of 8sec
    function loadPopup() {
        $("#loading").modal('show');
    }
    setTimeout(loadPopup, 8000);

    //Vertical Center Modal Script
    $('.modal').on('show.bs.modal', centerModals);
    $(window).on('resize', function () {
        $('.modal:visible').each(centerModals);
    });

    //Quick View Flyout
    $jq('.quickclick').on('click', function(){
        var openDivLen = $jq('.quickview-flyout-click[style*="display: block"]').length;
        if(openDivLen != "0" && !$jq(this).hasClass('active')){
            var oldVal = $jq('.quickview-flyout-click[style*="display: block"]').parent().find(".quickclick");
            $jq(oldVal).toggleClass('active');
            $jq(oldVal).parent().parent().parent().next('.quickview-flyout-click').slideToggle(400);
        }
        $jq(this).toggleClass('active');
        $jq(this).parent().parent().parent().next('.quickview-flyout-click').slideToggle(400);
        $('.carousel1, .fullslider1').resize();
    });
    $jq('.closequick').on('click', function () {
        $jq(this).parent().slideUp();
        $jq('.quickclick, .outer-layer, .product-qukclick').removeClass('active');
    });

    //Product Listing Page Quick View
    $jq('.product-qukclick').on('click', function(){
        var openFlyout = $jq('.product-quickview-flyout[style*="display: block"]').length;
        if(openFlyout != "0" && !$jq(this).hasClass('active')){
            var oldFly = $jq('.product-quickview-flyout[style*="display: block"]').parent().find(".product-qukclick");
            $jq(oldFly).closest('ul').parent().parent().parent().toggleClass('active');
            $jq(oldFly).toggleClass('active');
            $jq(oldFly).closest('ul').parent().parent().parent().next('.product-quickview-flyout').slideToggle(400);
        }
        $jq(this).closest('ul').parent().parent().parent().toggleClass('active');
        $jq(this).toggleClass('active');
        $jq(this).closest('ul').parent().parent().parent().next('.product-quickview-flyout').slideToggle(400);
        $('.carousel1, .fullslider1').resize();
    });

    //Touch and Not Touch Device
    if (isTouchDevice() === true) {
        //alert('Touch Device'); //your logic for touch device here
        $jq('.hide-notouch').show();
        $jq('.main-navigation li a').removeClass('disabled');
        $jq('#panoramaview').hide();
    } else {
        //alert('Not a Touch Device'); //your logic for non touch device here
        $jq('.hide-notouch').hide();
    }

    //Material Popup images Change
    $jq(".marterial-big-images img:eq(0)").nextAll().hide();
    $jq(".marterial-small-images img").on('click', function(e){
        e.preventDefault();
        var index = $jq(this).index();
        $jq(".marterial-big-images img").eq(index).show().siblings().hide();
    });
    $jq(".marterial-body").find("li").on('click', function(e) {
        $jq($jq(this).find("a").attr("href")).find(".marterial-big-images img:eq(0)").nextAll().hide();
        $jq($jq(this).find("a").attr("href")).find(".marterial-big-images img:eq(0)").show();
        $jq($jq(this).find("a").attr("href")).find(".marterial-small-images img").on('click', function(e){
            e.preventDefault();
            var index = $jq(this).index();
            $jq(this).parent().parent().find(".marterial-big-images img").eq(index).show().siblings().hide();
        });
    });

    //Uncheck Ship to same address
    $('input.ship-other').on('ifChanged', function(){
        $('.ship-to-other-addr').slideToggle(400);
    });

    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    //Easy Responsive Tabs
    $('.verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
    });

    //Custom Select
    $(".select").select2({
        minimumResultsForSearch: Infinity,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });

    //Custom Checkbox and Radio Button
    $(".checkbox input[type='checkbox']").iCheck({
        checkboxClass: 'icheckbox_minimal-purple'
    });
    $(".radio input[type='radio']").iCheck({
        radioClass: 'iradio_minimal-purple'
    });

    //Flex Slider
    $('.carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 10,
        asNavFor: '.slider'
    });
    $('.slider').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        sync: ".carousel"
    });

    //Bootstrap Rating
    /*Rating Read only*/
    $('.rating-display').rating({displayOnly: true, step: 0.5});
    /*Editable Rating*/
    $("rating").rating();

    // Softness
    $("#softness-slider").slider({
        range: "min",
        min: 0,
        max: 4,
        value: 1
    }).slider("pips", {
        first: "pip",
        last: "pip"
    });
    // Depth
    $("#depth-slider").slider({
        range: "min",
        min: 0,
        max: 4,
        value: 3
    }).slider("pips", {
        first: "pip",
        last: "pip"
    });
    // Height
    $("#height-slider").slider({
        range: "min",
        min: 0,
        max: 4,
        value: 2
    }).slider("pips", {
        first: "pip",
        last: "pip"
    });
    // Sit
    $("#sit-slider").slider({
        range: "min",
        min: 0,
        max: 4,
        value: 1
    }).slider("pips", {
        first: "pip",
        last: "pip"
    });

    //Photo Zoom
    $(".imageContainer").photoZoom();

    //Mixitup
    $('.post-filters').mixItUp({
        load: {
            filter: '.category-1'
        }
    });

    //Panorama
    var pan = document.getElementById('panoramaview');
    var PSV = new PhotoSphereViewer({
        panorama: 'http://tassedecafe.org/wp-content/uploads/2013/01/parc-saint-pierre-amiens.jpg',
        container: pan,
        time_anim: 3000,
        navbar: true,
        navbar_style: {
            backgroundColor: 'rgba(58, 67, 77, 0.7)'
        }
    });
});

/*Autocomplete*/
$(function () {
    var availableTags = [
        "Leather sofas",
        "Leather chairs",
        "Leather in Living",
        "Leatherette"
    ];
    $(".types").autocomplete({
        source: availableTags
    });
});

/* Vertical Center Modal Function Script */
function centerModals() {
    var modal = $(this),
        dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}

/*Google Map*/
function initialize() {
    var mapProp = {
        center: new google.maps.LatLng(51.508742, -0.120850),
        zoom: 5,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);

$(window).load(function () {
    //Quick view Slider /  Pre order Slider
    $('.carousel1').each(function(index){
        $('.carousel1').eq(index).flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 10,
            asNavFor: '.fullslider1:eq('+index+')'
        });
    });
    //Quick view Slider /  Pre order Slider
    $('.fullslider1').each(function(index){
        $('.fullslider1').eq(index).flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: '.carousel1:eq('+index+')'
        });
    });

    //Material select Slider
    $('.diff-material').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 42,
        itemMargin: 8,
        slideshow: false,
        controlNav: false,
        minItems: 3,
        maxItems: 6
    });

    //Swatches select Slider in fabric selection
    $('.swatches-samples').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 45,
        itemMargin: 10,
        slideshow: false,
        controlNav: false,
        minItems: 3,
        maxItems: 8
    });

    //Pins look Slider
    $('.slider1').flexslider({
        animation: "slide",
        animationLoop: false,
        slideshow: false,
        itemWidth: 282,
        controlNav: false,
        minItems: 1,
        maxItems: 4
    });

});

/*Send to store slider*/
function initCloseModal(){
    $('.closestore-slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false
   });
}

function isTouchDevice() {
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

