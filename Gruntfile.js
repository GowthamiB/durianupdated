// Gruntfile.js
// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function

module.exports = function(grunt) {

    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({

        // get the configuration info from package.json ----------------------------
        // this way we can use things like name and version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),

        // SASS to CSS Compile
        sass: {
            dist: {
                files: {
                    'dist/assets/css/vendor.css': 'src/sass/vendor.scss',
                    'dist/assets/css/styles.css': 'src/sass/styles.scss'
                }
            }
        },

        //Css Minify
        cssmin: {
            minify: {
                files: {
                    'dist/assets/css/vendor.min.css': 'dist/assets/css/vendor.css',
                    'dist/assets/css/styles.min.css': 'dist/assets/css/styles.css'
                }
            }
        },

        // Copy Files
        copy: {
           files2: {
                expand: true,
                src: ['bower_components/bootstrap/dist/fonts/*', 'bower_components/font-awesome/fonts/*', 'bower_components/flexslider/fonts/*'],
                dest: 'dist/assets/fonts',
                flatten: true,
                filter: 'isFile'
            }
        },

        //Concat
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/jquery-ui/jquery-ui.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'src/js/vendor/responsive-tabs/responsive-tabs.js',
                    'src/js/vendor/select2/select2.min.js',
                    'src/js/vendor/icheck/icheck.min.js',
					'bower_components/flexslider/jquery.flexslider-min.js',
                    'bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js',
                    'bower_components/bootstrap-star-rating/js/star-rating.min.js',
                    'bower_components/jQuery-ui-Slider-Pips/dist/jquery-ui-slider-pips.min.js',
                    'src/js/vendor/photozoom/photoZoom.js',
                    'bower_components/mixitup/build/jquery.mixitup.min.js',
                    'src/js/vendor/panorama/three.min.js',
                    'src/js/vendor/panorama/photo-sphere-viewer.min.js'
                ],
                dest: 'src/js/vendor.js'
            }
        },

        //Js Minify
        uglify: {
            my_target: {
                files: {
                    'dist/assets/js/vendor.min.js': 'src/js/vendor.js'
                }
            }
        },

        watch: {
            files: ['dist/*.html','src/sass/*.scss','dist/assets/js/*.js'],
            tasks: ['sass', 'cssmin', 'uglify']
        }


    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('default', ['sass', 'cssmin', 'copy', 'uglify', 'concat', 'watch']);

};